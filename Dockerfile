FROM node:slim AS build

WORKDIR /usr/local/src
COPY package*.json ./
RUN npm ci

FROM node:slim AS production
COPY --from=build /usr/local/src/node_modules node_modules
COPY . .
EXPOSE 3000
CMD ["node", "server.js"]
