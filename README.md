## Dummy application showing how drone builds and updates an application

### How drone works
1. Drone-Server runs in Server A. Drone-Exec-Runner runs in Server B
2. Drone-Server listens to github / gitlab for pushes with `.drone.yml` file. 
3. As soon as a push happens, Drone-Server pulls in the github repo and runs the `drone.yml` file
4. Drone-Server and Drone-Exec-Runner are both docker containers. Drone-Server controls Drone-Exec-Runner by sending it commands

### How to run
1. Get a remote serverA where you want to deploy Drone-Server
2. Create an application on github / gitlab and get their secrets and application ID. Also enter the callback url to be the IP address that Drone-Server is on
3. Once done, take the secret and applicationID and insert into the command below. 
```
docker run \
  --volume=/var/lib/drone:/data \
  --env=DRONE_GITLAB_SERVER=https://gitlab.com \
  --env=DRONE_GITLAB_CLIENT_ID={{GITLAB_CLIENT_ID}} \
  --env=DRONE_GITLAB_CLIENT_SECRET={{GITLAB_SECRET}} \
  --env=DRONE_RPC_SECRET={{RPC_SECRET}} \    // a random generated secret that is shared between drone-server and drone-runner
  --env=DRONE_GITLAB_PRIVATE_MODE=true \    // to allow drone to read private repos
  --env=DRONE_SERVER_HOST=35.xxx.xxx.170:80 \  // the IP address or the domain name where drone resides. 
  --env=DRONE_SERVER_PROTO=http \
  --publish=80:80 \
  --publish=443:443 \
  --restart=always \
  --detach=true \
  --name=drone \
  drone/drone:latest
```

### The basic CI / CD flow
1. Drone pulls from gitlab
2. Drone runs the drone-exec-runner
// see cloudbuild.yaml
3. Drone-Exec-Runner builds a docker image using google-cloud builder
4. The image is stored in GCR
5. Drone-Exec-Runner pulls the image using `docker-compose pull`
6. Once pulled, Drone-Exec-Runner runs the image using `docker-compose up --force-recreate` to redeploy the image

### Points to consider
1. Drone requires credentials to talk to gcloud. Thus, a hardcoding of secrets in the server is needed. This is a security concern
2. Drone is running as a process on the server and not in a container. Thus there is no isolation and the server is exposed to direct commands
3. Drone to run in protected branches and limited to only certain branch updates. https://discourse.drone.io/t/have-builds-only-run-when-a-certain-branch-is-updated/6438/29
4. Drone to link up to vault for secrets
5. Drone to run not in exec mode, but as a docker-container that has access to google services. Build a drone custom plugin.
